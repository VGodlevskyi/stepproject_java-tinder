## StepProject_java-tinder

# Step Project [JAVA-Tinder](http://app-java-tinder.herokuapp.com/)

### `Nika Pavlenko - FE-18` [GitLab]()
###  `Vitalii Godlevskyi - FE-18` [GitLab](https://gitlab.com/VGodlevskyi) / [GitHub](https://github.com/VGodlevskyi)
### `Ihor Ivliev - FE-18` [GitLab]() / [GitHub]()

#Local URL
http://localhost:8080/

#User example for testing
login:    np@gmail.com
password: 123

#Deploy on Heroku
http://app-java-tinder.herokuapp.com/

#Database
dbconnection JDBC_DATABASE_URL=jdbc:postgresql://ec2-54-160-96-70.compute-1.amazonaws.com:5432/d3f5i1si31cu2r?password=08e82ee554cc6d7797e7ddbc5967426b462960fe4e28fccfaeb7f294e1d1df35&sslmode=require&user=bzqpbltonvtiqf

 